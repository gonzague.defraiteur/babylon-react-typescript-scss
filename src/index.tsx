import * as React from "react";
import * as ReactDOM from "react-dom";
import Tiles from "./Tiles";

const data = [{
    id: 1,
    name: "fennec",
    folder: "resources/3d/fennec_fox/",
    fileName: "scene.gltf",
    invertedNormals: true,
  }, {
    id: 2,
    name: "heavy_bot",
    folder: "resources/3d/heavy_bot/",
    fileName: "scene.gltf",
    invertedNormals: false,
  }, {
    id: 3,
    name: "media_bot",
    folder: "resources/3d/media_bot/",
    fileName: "scene.gltf",
    invertedNormals: false,
  }, {
    id: 4,
    name: "random_guy",
    folder: "resources/3d/random_guy/",
    fileName: "scene.gltf",
    invertedNormals: false,
  }];

ReactDOM.render(
  <React.StrictMode>
    <Tiles data={data} />,
  </React.StrictMode>,
  document.getElementById('root')
);
