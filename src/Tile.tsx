import * as React from 'react'
import BabylonView from './BabylonView';
import './Tile.scss';

interface Props {
	data: any
}
interface State
{
	open: boolean,
	mouseOver: boolean
}

class Tile extends React.Component<Props, State> {
	constructor(props) {
		super(props);
		this.state = {
		  open: false,
		  mouseOver: false
		};
		this._clickHandler = this._clickHandler.bind(this);
		this._mouseEnter = this._mouseEnter.bind(this);
		this._mouseLeave = this._mouseLeave.bind(this);
	  }
	_mouseEnter(e) {
	  if ((this.state as any).mouseOver === false) {
		this.setState({
		  mouseOver: true
		})
	  }
	}
	_mouseLeave(e) {
	  if ((this.state as any).mouseOver === true) {
		this.setState({
		  mouseOver: false
		})
	  }
	}
	_clickHandler(e) {
	  if ((this.state as any).open === false) {
		this.setState({
		  open: true
		});
	  } else {
		this.setState({
		  open: false
		});
	  }
	}
	hasMouseOver = false;
	render() {
	  let tileStyle = {};

	  if ((this.state as any).open) {
		tileStyle = {
		  width: '62vw',
		  height: '62vw',
		  position: 'fixed',
		  top: '50%',
		  left: '50%',
		  margin: '0',
		  marginTop: '-31vw',
		  marginLeft: '-31vw',
		  boxShadow: '0 0 40px 5px rgba(0, 0, 0, 0.3)',
		  transform: 'none'
		};
	  } else {
		tileStyle = {
		  width: '18vw',
		  height: '18vw'
		};
	  }
	  return (
		<div className="tile">
			 <BabylonView onPointerEnter = {this._mouseEnter}
			  onPointerLeave = {this._mouseLeave}
			  onClick = {this._clickHandler}
			  style={tileStyle}
			  fileSrc={(this.props as any).data.fileName}
			  filePath={(this.props as any).data.folder}
			  invertedNormals={(this.props as any).data.invertedNormals}
			  state={this.state}
			  />
		</div>
	  );
	}
  }
  

 export default Tile;
