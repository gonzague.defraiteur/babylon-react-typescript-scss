import React from 'react';
import { Engine } from '@babylonjs/core/Engines/engine'
import { EngineOptions } from '@babylonjs/core/Engines/thinEngine'
import { Scene, SceneOptions } from '@babylonjs/core/scene'
import { EngineCanvasContext, EngineCanvasContextType, SceneContext, SceneContextType } from 'babylonjs-hook';

export type BabylonjsProps = {
    antialias?: boolean
    engineOptions?: EngineOptions
    adaptToDeviceRatio?: boolean
    renderChildrenWhenReady?: boolean
    sceneOptions?: SceneOptions
    onSceneReady: (scene: Scene) => void
    onRender?: (scene: Scene) => void
    state: {open: boolean,
    mouseOver: boolean}
    id: string
    children?: React.ReactNode
};

class BabySceneState
{
    engineContext: EngineCanvasContextType;
    sceneContext: SceneContextType;
}

class BabySceneComponent extends React.Component<BabylonjsProps, BabySceneState>
{
    reactCanvas: HTMLCanvasElement;
    onAfterRender: () => void;
    constructor(props) {
		super(props);
        this.state ={engineContext: null, sceneContext: null}
        this.Initialize();
	}
    Initialize() {
    }
    componentDidMount()
    {
        this.canvasLoaded();
    }
    render()
    {
        var self = this;
        var reactCanvas =  (<canvas ref={(ref) => {self.reactCanvas = ref;
            
        }} /> as any  as HTMLCanvasElement);

        return (

            <>
           {reactCanvas}
            
            <EngineCanvasContext.Provider value={this.state.engineContext}>
                <SceneContext.Provider value={this.state.sceneContext}>
                    {
                    (this.props.renderChildrenWhenReady !== true || (this.props.renderChildrenWhenReady === true && this.state.sceneContext.sceneReady)) &&
                        this.props.children
                    }
                </SceneContext.Provider>
            </EngineCanvasContext.Provider>
        </>
        );
    }    
    canvasLoaded() {

        const engine = new Engine(this.reactCanvas, this.props.antialias, this.props.engineOptions, this.props.adaptToDeviceRatio);
        var engineContext = {engine: engine, canvas: this.reactCanvas};
        
        this.setState({engineContext: engineContext});
        const scene = new Scene(engine, this.props.sceneOptions);
        const sceneIsReady = scene.isReady();
        if (sceneIsReady) {
            this.props.onSceneReady(scene);

        } else {
            scene.onReadyObservable.addOnce((scene) => {
                this.props.onSceneReady(scene);
                this.setState({sceneContext: {
                    canvas: this.reactCanvas,
                    scene,
                    engine,
                    sceneReady: true,
                } as any});
            });
        }

        engine.runRenderLoop(() => 
            {
                if (scene.activeCamera) {
                    if (typeof this.props.onRender === 'function') {
                        this.props.onRender(scene);
                    }
                    scene.render();

                    
                } else {
                    console.warn('no active camera..');
                }
                if (this.onAfterRender != undefined)
                {
                    console.log("i have after render...");
                    this.onAfterRender();
                }
            
        })

        const resize = () => {
            scene.getEngine().resize();
        }

        if (window) {
            window.addEventListener('resize', resize);
        }

    }
}


export default BabySceneComponent;