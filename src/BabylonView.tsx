import * as React from 'react';
import { FreeCamera, Vector3, HemisphericLight, MeshBuilder } from '@babylonjs/core';
import * as BABYLON from "@babylonjs/core";
import "@babylonjs/loaders/glTF";
import BabySceneComponent from './SceneComponent';
import './BabylonView.scss';


interface Props {
	onPointerEnter: (app: BabylonView) => void;
	onPointerLeave: (app: BabylonView) => void;
	onClick: (app: BabylonView) => void;
	style: {};
	fileSrc: string;
	filePath: string;
	invertedNormals: boolean;
	state: { open: boolean,
		mouseOver: boolean}
}
interface State {

}

class BabylonView extends React.Component<Props, State> {
	myRef: any = undefined;
	scene: BABYLON.Scene;
	scnComp: BabySceneComponent;
	constructor(props) {
		super(props);
		this.state = {
			open: false,
			mouseOver: false
		};

		this._clickHandler = this._clickHandler.bind(this);
		this._mouseEnter = this._mouseEnter.bind(this);
		this._mouseLeave = this._mouseLeave.bind(this);

	}

	_mouseEnter(e) {
		e.preventDefault();
		if ((this.state as any).mouseOver === false) {
			console.log("mouse enter...");
			this.setState({
				mouseOver: true
			})
		}
	}
	_mouseLeave(e) {
		e.preventDefault();
		if ((this.state as any).mouseOver === true) {
			this.setState({
				mouseOver: false
			})
		}
	}
	_clickHandler(e) {
		e.preventDefault();
		if ((this.state as any).open === false) {
			this.setState({
				open: true
			});
		} else {
			this.setState({
				open: false
			});
		}
	}
	box: BABYLON.Mesh;
	canvas: HTMLCanvasElement;
	onSceneReady(scene: BABYLON.Scene) {
		this.scene = scene;
		var camera = new FreeCamera("camera1", new Vector3(0, 5, -10), scene);
		camera.setTarget(Vector3.Zero());
		var self = this;
		BABYLON.SceneLoader.Append(this.props.filePath, this.props.fileSrc, scene, function (scene) {

			scene.createDefaultCameraOrLight(true, true, true);
			var iteration = 0;
			scene.onReadyObservable.addOnce((scene) => {
					BABYLON.Tools.CreateScreenshot(scene.getEngine(), camera, { width: 300, height: 300 }, function (data) {
						var img = document.createElement("img");
						img.src = data;
						document.body.appendChild(img);	
					});
					self.scnComp.onAfterRender = undefined;
            });
			self.scnComp.onAfterRender = () => {
				
			}
			
		});

		const canvas = scene.getEngine().getRenderingCanvas();
		this.canvas = canvas;
		canvas.style.width = "100%";
		canvas.style.height = "100%";
		canvas.width = 2048;
		canvas.height = 2048;
		canvas.onpointerenter = (evt) => {
			this.props.onPointerEnter(this);
		};
		canvas.onpointerleave = (evt) => {

			this.props.onPointerLeave(this);
		}
		canvas.onclick = (evt) => {
			this.props.onClick(this);
		}

		camera.attachControl(canvas, false);

		var light = new HemisphericLight("light", new Vector3(0, 1, 0), scene);

		light.intensity = 0.7;

		this.box = MeshBuilder.CreateBox("box", { size: 2 }, scene);

		this.box.position.y = 1;

		MeshBuilder.CreateGround("ground", { width: 6, height: 6 }, scene);

	}

	onRender(scene: BABYLON.Scene) {
		if (this.box !== undefined) {
			var deltaTimeInMillis = scene.getEngine().getDeltaTime();

			const rpm = 10;
			this.box.rotation.y += ((rpm / 60) * Math.PI * 2 * (deltaTimeInMillis / 1000));
		}
	}
	paused: boolean = false;
	render() {
		var scnComp = <BabySceneComponent ref={(ref) => { this.scnComp = ref}} engineOptions={{preserveDrawingBuffer: true, preserveStencilBuffer: true, stencil: true} as any} state={this.props.state} antialias onSceneReady={(scene0) => { this.onSceneReady(scene0) }} onRender={(scene) => { this.onRender(scene) }} id='my-canvas' />;

		this.myRef = scnComp;
		return (
			<div style={this.props.style} className="App">
				<header className="App-header">
					{scnComp}
				</header>
			</div>
		);
	}
}

export default BabylonView;
